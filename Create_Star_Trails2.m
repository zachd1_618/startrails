function Blended_Image = Create_Star_Trails2(InPath,Threshold)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                        January 12, 2012
%                                                   v0.3
%      Zach Dischner
%
%     Function  :
%
%     Purpose   :
%
%     Input     :
%
%     Output    :
%
%     Procedure :
%
%
%    Notes/TODOS
%                           Redo Waitbar
%                           When finished, modularize everythign into
%                           funcitons
%                           Grab metadata, append modified data to finished
%                            result
%
%   History/Past Revisions:
%               v0.1:   Simple average of images. Worked, but ate up a lot
%                          of memory and star trails ended up disapearing
%                          after many  iterations  because we were
%                          averaging stars with background. No
%                          Bueno
%
%               v0.2:   This time, we only add portions of an image that
%                          are above a defined threshold. This works much
%                          better. Not a memory hog, faster, and keeps to
%                          the true image much better. But the final image
%                          is very harsh, as we are just adding [0 0 0 0 1]
%                          End result is pixelated and very 'digital'
%                          looking
%
%               v0.3:  NOW try to employ moving average to the Thresholded
%                         image. This should essentially soften the added
%                         stars, so the end result is much more defocused.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Prepare Workspace
clc, close all

disp('Make sure you only have images in a single sequence in the directory. Every image will be searched')

%% Find Images
% Add input here to check for file types
filenames    = dir([InPath  '/*.jpg']);


%% Define a filter (Guesswork at this point)
% Lets try an average over 3 pixel
average_filter  = fspecial('average',3);
% or a blurring filter over 3 pixels
blur_filter= fspecial('disk',3);   % this is a blurring filter

filter = blur_filter;
for kk = 1:length(Threshold)
    h                   = waitbar(0,'Magical things are happening...');
    
    % Load the first image in the directory. This is the image that all the rest will be added to
    Base_Image   = im2double(imread([InPath '/' filenames(1).name]));
    temp_image  = zeros(size(Base_Image));
    
    %% Perform Stacking for Each Image in the Directory
    for ii = 2:length(filenames)
        % Update waitbar
        waitbar(ii/length(filenames),h)
        
        % Load new image into temporary variable
        temp_image = im2double(imread([InPath '/' filenames(ii).name]));
        temp_gray     = rgb2gray(temp_image);
        
        % Apply Thresholding filter to the temporary image
        temp_image = temp_image.*repmat(temp_gray > Threshold(kk),[1,1,3]);
        
        % Apply image filter. Define earlier for different filter choices/algorithms
        temp_image = imfilter(temp_image,filter);
        
        % Now add the new stars to the base image (which becomes the new base image
        Base_Image   = imadd( Base_Image, temp_image);
        
    end
    
    if fileattrib([InPath '/' 'completed']) == 0
        mkdir([InPath '/' 'completed'])
    end
    imwrite(Base_Image,[InPath '/completed/BlendedImage_Threshold_' num2str(Threshold(kk))  '.jpg'],...
        'JPEG','Quality',100);
    
    Blended_Image = Base_Image;
    imshow(Blended_Image);
    title('Blended image')
    
    figure
    Avg_image = imfilter(Blended_Image,average_filter);
    imshow(Avg_image)
    
    imwrite(Avg_image,...
        [InPath '/completed/BlendedImage_AVG_BLUR_Threshold_' num2str(Threshold(kk))  '.jpg'],...
        'JPEG','Quality',100);
    title('Blended with a 3 pixel averaging filter applied to soften')
    
    figure
    Blur_image = imfilter(Blended_Image,blur_filter);
    imshow(Blur_image);

    imwrite(Blur_image,...
        [InPath '/completed/BlendedImage_AVG_BLUR_Threshold_' num2str(Threshold(kk))  '.jpg'],...
        'JPEG','Quality',100);
    
    title('Blended with a 3 pixel radial blur filter applied to soften')
    close(h)
end



end
