function  Threshold = Find_Star_Threshold(imagepath,Threshold_Range)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           January 12, 2012
%      Zach Dischner
%
%     Function  :
%
%     Purpose   :
%
%     Input     :   
%                         imagepath: Path to image for analysis, with image name included
%                         Threshold_Range: Linear array of threshold values to examine image over
%                                                         Think of the values as a decimal percent of an overblown
%                                                         signal. [.25] means 25% of a unity signal
%
%     Output    :
%
%     Procedure :
%
%
%    Notes/TODOS
%               Add Statistical analysis to predict best value
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Prepare workspace and Verify Inputs
clc; close all

if exist(imagepath,'file') == 0
    disp(['Oh SNAP! The imagepath you specified does not lead to an image'])
    disp(['Please check that <' imagepath '> points to an image'])
    return
end

if length(size(Threshold_Range)) > 2 || max(size(Threshold_Range) == 1) ==0
    disp('Your Threshold_Range variable is not the right size. Please use single vector')
end

%% Prepare for Comparison
% Load Base Image
disp(['Loading Image for analysis'])
base_image = im2double(imread(imagepath));

% Resize image for speed and viewability
screen_size = get(0,'ScreenSize');
sw = screen_size(3);    % Screen Width
sh = screen_size(4);    % Screen Height

image_size = size(base_image);
IMw     = image_size(2);    % Image Width
IMh      = image_size(1);    % Image Height'

% Find scaling factor to better fit the screen
Factor = min([sh/2/IMh sw/2/IMw]);

% Apply Scaling factor
base_image = imresize(base_image,Factor,'Nearest');   %Not the best resize method, but fast. Thats what matters here


% Convert to Grayscale for easier threshold signal detection
base_gray     = rgb2gray(base_image);
% imshow(base_image)

h = waitbar(0,'Performing Threshold Analysis');
for ii = 1:length(Threshold_Range)
    waitbar(ii/length(Threshold_Range))
    % Single Plot to Compare Threshold results
        figure
        
    imshow([base_image base_image.*repmat(base_gray > Threshold_Range(ii),[1,1,3])]);
    title(['Threshold Value = ' num2str(Threshold_Range(ii))])
end


%% Lets try some statistical analysis
Augmented_Range = linspace(Threshold_Range(1), Threshold_Range(end));
for ii = 1:length(Augmented_Range)
    image_std (ii) = mean(mean(std((base_image.*repmat(base_gray > Augmented_Range(ii),[1,1,3])) > 0)));
end


% Find Rate of Change of the standard deviation
ROC_std  = diff(image_std);
min_ROC = min(abs(ROC_std));
ideal_threshold_index = find(abs(ROC_std) == min_ROC);
Threshold = Augmented_Range(ideal_threshold_index);
disp(['Apparently, the ideal threshold you should use is     ' Threshold])

figure
plot(Augmented_Range,image_std)
hold on
plot(Threshold,image_std(ideal_threshold_index)...
          ,'rs','MarkerSize',4,'MarkerFaceColor','r','MarkerEdgeColor','k')
title('Standard Deviation of SIGNAL over Threshold Range')
xlabel('Threshold value'); ylabel('Standard Deviation of the resulting Signal')
close(h)

